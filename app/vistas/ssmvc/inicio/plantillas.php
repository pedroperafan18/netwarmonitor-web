<div id="Templates" class="hidden">
    <table id="templateContacto">
        <tr>
            <td>
                {{Nombre}}
            </td>
            <td>
                {{Correo}}
            </td>
            <td>
                {{Telefono}}
            </td>
            <td>
                {{Estado}}
            </td>
            <td>
                {{Municipio}}
            </td>
            <td>
                <a class="editar" data-id="{{ID}}" href="#">Editar</a>
            </td>
        </tr>
    </table>

    <table id="templateCita">
        <tr>
            <td>
                {{Contacto}}
            </td>
            <td>
                {{Fecha}}
            </td>
            <td>
                {{Hora}}
            </td>
            <td>
                {{Duracion}}
            </td>
        </tr>
    </table>
</div>