<div class="row">
    <div id="divOpciones" class="col-md-6">
        <div class="jumbotron">
            <h4>
                <a id="agregarContacto" href="#">
                    Agregar contacto
                </a>
            </h4>
            <h4>
                <a id="aVerContactos" href="#">
                    Ver lista de contactos
                </a>
            </h4>
            <h4>
                <a id="agregarCita" href="#">
                    Agregar citas
                </a>
            </h4>
            <h4>
                <a id="reporteCitas" href="#">
                    Reporte citas
                </a>
            </h4>
        </div>
    </div>

    <div id="divRegresar" class="col-md-12 hidden">
        <div class="btn btn-primary">Regresar</div>
    </div>

    <div id="divReporte" class="col-md-6 col-md-offset-3 hidden">
        <div class="well">
            <form id="formReporte" class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Fecha Inicial</label>
                    <div class="col-sm-5">
                        <input type="date" class="form-control" name="FechaInicial" id="FechaInicial" placeholder="dd/mm/aaaa">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Fecha Final</label>
                    <div class="col-sm-5">
                        <input type="date" class="form-control" name="FechaFinal" id="FechaFinal" placeholder="dd/mm/aaaa">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary" id="generarReporte">Generar reporte</button>
            </form>
        </div>

        <div id="tablaResultadosReporte" class="well hidden">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Contacto</th>
                    <th>Fecha</th>
                    <th>Hora</th>
                    <th>Duración</th>
                </tr>
                </thead>
                <tbody id="resultadosReporte">

                </tbody>
            </table>
        </div>
    </div>

    <div id="divCrearCita" class="col-md-6 col-md-offset-3 well hidden">
        <form id="formCita" class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 control-label">Contacto</label>
                <div class="col-sm-10">
                    <select name="Contacto" id="Contacto" class="form-control">
                        <option value="0"></option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Fecha</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" name="Fecha" id="Fecha" placeholder="dd/mm/aaaa">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Hora</label>
                <div class="col-sm-10">
                    <input type="time" class="form-control" name="Hora" id="Hora" placeholder="00:00">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Duración</label>
                <div class="col-sm-10">
                    <select name="Duracion" id="Duracion" class="form-control">
                        <option value="0"></option>
                        <option value="1">30 minutos</option>
                        <option value="2">1 hora</option>
                        <option value="3">1 hora y 30 minutos</option>
                        <option value="4">2 horas</option>
                        <option value="5">más de 2 horas</option>
                    </select>
                </div>
            </div>
            <button type="submit" class="btn btn-primary" id="guardarCita">Crear cita</button>
        </form>
    </div>

    <div id="divCrearContacto" class="col-md-6 col-md-offset-3 well hidden">
        <form id="formContacto" class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-4 control-label">Nombre completo*</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="Nombre" id="Nombre" placeholder="Nombre">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Teléfono</label>
                <div class="col-sm-8">
                    <input maxlength="10" type="text" class="form-control" name="Telefono" id="Telefono" placeholder="Teléfono">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-8 col-sm-offset-4">
                    <select name="TipoTelefono" id="TipoTelefono" class="form-control">
                        <option value="1">Móvil</option>
                        <option value="2">Trabajo</option>
                        <option value="3">Casa</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Dirección de correo</label>
                <div class="col-sm-8">
                    <input type="email" class="form-control" name="Correo" id="Correo" placeholder="Dirección de correo">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Estado</label>
                <div class="col-sm-8">
                    <select name="Estado" id="Estado" class="form-control">
                        <option value="0"></option>
                        <?php
                        foreach ($estados as $estado){
                        ?>
                            <option value="<?php echo $estado["id"];?>">
                                <?php echo $estado["estado"];?>
                            </option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Municipio</label>
                <div class="col-sm-8">
                    <select name="Municipio" id="Municipio" class="form-control">
                    </select>
                </div>
            </div>
            <button type="submit" class="btn btn-primary" id="guardarContacto">Crear contacto</button>
            <button type="submit" class="btn btn-primary hidden" id="editarContacto">Editar contacto</button>

        </form>
    </div>

    <div id="divListaContactos" class="col-md-8 col-md-offset-2 hidden">
        <table id="tablaContactos" class="table table-striped">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>Telefono</th>
                    <th>Estado</th>
                    <th>Municipio</th>
                    <th></th>
                <tr>
                    <th><input type="text" class="form-control"/></th>
                    <th><input type="text" class="form-control"/></th>
                    <th><input type="text" class="form-control"/></th>
                    <th><input type="text" class="form-control"/></th>
                    <th><input type="text" class="form-control"/></th>
                    <th></th>
                </tr>
            </thead>
            <tbody id="listaContactos">

            </tbody>
        </table>
    </div>
</div>

<?php
echo $plantillas;
?>

