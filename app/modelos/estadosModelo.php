<?php
/**
 * Created by PhpStorm.
 * User: perafan
 * Date: 4/07/17
 * Time: 12:53 AM
 */

class estadosModelo extends Modelo
{

    function __construct()
    {
        $this->baseDeDatos = $this->loadDatabase("MySQL");
    }

    function getAll(){
        $resultado = $this->baseDeDatos->query("SELECT * FROM estados");
        return mysqli_fetch_all($resultado,MYSQLI_ASSOC);
    }

}