<?php

class inicioModelo extends Modelo
{
    private $baseDeDatos;
    function __construct()
    {
        $this->baseDeDatos = $this->loadDatabase("MySQL");
    }

    function getMunicipios($id){
        $resultado = $this->baseDeDatos->query("SELECT municipios.id, municipio FROM estados_municipios INNER JOIN municipios ON estados_municipios.municipios_id = municipios.id WHERE estados_id = '$id'");
        return mysqli_fetch_all($resultado,MYSQLI_ASSOC);
    }
}