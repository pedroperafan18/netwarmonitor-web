<?php

/**
 * Created by PhpStorm.
 * User: perafan
 * Date: 2/07/17
 * Time: 10:40 PM
 */
class contactoModelo extends Modelo
{

    function __construct()
    {
        $this->baseDeDatos = $this->loadDatabase("MySQL");
    }

    function getAll(){
        $sql = "SELECT contactos.ID, contactos.Nombre,contactos.Telefono,
                contactos.TipoTelefono,contactos.Correo, 
                estados.estado as Estado ,municipios.municipio as Municipio 
                FROM contactos 
                LEFT JOIN estados ON contactos.Estado = estados.id 
                LEFT JOIN municipios ON contactos.Municipio = municipios.id";
        $resultado = $this->baseDeDatos->query($sql);
        return mysqli_fetch_all($resultado,MYSQLI_ASSOC);
    }

    function getOne($id){
        $sql = "SELECT *
                FROM contactos 
                WHERE contactos.ID ='$id'";
        $resultado = $this->baseDeDatos->query($sql);
        return mysqli_fetch_all($resultado,MYSQLI_ASSOC);
    }

    function create($data){
        $nombre = $data["Nombre"];
        $telefono = $data["Telefono"];
        $tipoTelefono = $data["TipoTelefono"];
        $correo = $data["Correo"];
        $estado = $data["Estado"];
        $municipio = $data["Municipio"];
        $sql = "INSERT INTO contactos SET 
                Nombre='$nombre', Telefono='$telefono',
                TipoTelefono='$tipoTelefono',Correo='$correo',
                Estado='$estado',Municipio='$municipio'";
        return $this->baseDeDatos->query($sql);
    }

    function update($data,$id){
        $nombre = $data["Nombre"];
        $telefono = $data["Telefono"];
        $tipoTelefono = $data["TipoTelefono"];
        $correo = $data["Correo"];
        $estado = $data["Estado"];
        $municipio = $data["Municipio"];
        $sql = "UPDATE contactos SET 
                Nombre='$nombre', Telefono='$telefono',
                TipoTelefono='$tipoTelefono',Correo='$correo',
                Estado='$estado',Municipio='$municipio' WHERE ID='$id'";
        /*
        $sql = "INSERT INTO contactos SET
                Nombre='$nombre', Telefono='$telefono',
                TipoTelefono='$tipoTelefono',Correo='$correo',
                Estado='$estado',Municipio='$municipio'";
        */
        return $this->baseDeDatos->query($sql);
    }

}