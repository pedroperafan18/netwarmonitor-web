<?php
/**
 * Created by PhpStorm.
 * User: perafan
 * Date: 5/07/17
 * Time: 12:05 AM
 */
class citaModelo extends Modelo
{

    function __construct()
    {
        $this->baseDeDatos = $this->loadDatabase("MySQL");
    }

    function create($data){
        $duracion = $data["Duracion"];
        $hora = $data["Hora"];
        $fecha = $data["Fecha"];
        $contacto = $data["Contacto"];
        $sql = "INSERT INTO citas SET 
                Contacto='$contacto', Fecha='$fecha',
                Hora='$hora',Duracion='$duracion'";
        return $this->baseDeDatos->query($sql);
    }

    function reporte($data){
        $fechaFinal = $data["FechaFinal"];
        $fechaInicial = $data["FechaInicial"];
        $sql = "SELECT * FROM citas INNER JOIN contactos ON citas.ID=contactos.ID WHERE Fecha>='$fechaInicial' AND Fecha <= '$fechaFinal'";
        $resultado = $this->baseDeDatos->query($sql);
        return mysqli_fetch_all($resultado,MYSQLI_ASSOC);
    }
}