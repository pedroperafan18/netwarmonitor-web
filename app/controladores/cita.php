<?php

class cita extends Controlador{

    public function create(){
        $error = null;
        if($_POST["Duracion"] == null){
            $error = "La duración es un campo obligatorio";
        }

        if($_POST["Hora"] == null){
            $error = "La hora es un campo obligatorio";
        }

        if($_POST["Fecha"] == null){
            $error = "La fecha es un campo obligatorio";
        }

        if($_POST["Contacto"] == null){
            $error = "El contacto es un campo obligatorio";
        }

        if($error != null){
            $this->json(
                array(
                    "Resultado" => false,
                    "Mensaje" => $error
                )
            );
        }


        $modeloCita = $this->modelo("citaModelo");
        $save_contacto = $modeloCita->create($_POST);

        $this->json(
            array(
                "Resultado" => $save_contacto,
                "Mensaje" => "Cita guardado correctamente"
            )
        );
    }

    public function reporte(){
        $error = null;
        if($_POST["FechaFinal"] == null){
            $error = "La fecha final es un campo obligatorio";
        }

        if($_POST["FechaInicial"] == null){
            $error = "La fecha inicial es un campo obligatorio";
        }

        if($error != null){
            $this->json(
                array(
                    "Resultado" => false,
                    "Mensaje" => $error
                )
            );
        }

        $modeloCita = $this->modelo("citaModelo");
        $citas = $modeloCita->reporte($_POST);

        $this->json(
            array(
                "Resultado" => true,
                "Citas" => $citas
            )
        );
    }

}