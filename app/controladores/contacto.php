<?php

class contacto extends Controlador{

    public function getOne(){

        $segmentos = Router::getInstance()->getSegmentos();
        $id_contacto = $segmentos[1];

        $modeloContacto = $this->modelo("contactoModelo");
        $contacto = $modeloContacto->getOne($id_contacto);

        $this->json(array(
                "Resultado" => $contacto[0]
            )
        );
    }

    public function getAll(){

        $modeloContacto = $this->modelo("contactoModelo");
        $contactos = $modeloContacto->getAll();

        $this->json(array(
                "Resultado" => $contactos
            )
        );
    }

    public function create(){
        $error = "Ocurrio algun error al guardar el contacto";
        if($_POST["Nombre"] == null){
            $error = "El nombre completo es un campo obligatorio";
        }

        $modeloContacto = $this->modelo("contactoModelo");
        $save_contacto = $modeloContacto->create($_POST);

        $mensaje = ($save_contacto)? "Contacto guardado correctamente":$error;
        $this->json(
            array(
                "Resultado" => $save_contacto,
                "Mensaje" => $mensaje
            )
        );
    }

    public function update(){
        parse_str(file_get_contents('php://input'), $_PUT);
        $segmentos = Router::getInstance()->getSegmentos();
        $id_contacto = $segmentos[1];

        $error = "Ocurrio algun error al guardar el contacto";
        if($_PUT["Nombre"] == null){
            $error = "El nombre completo es un campo obligatorio";
        }
        $modeloContacto = $this->modelo("contactoModelo");
        $save_contacto = $modeloContacto->update($_PUT,$id_contacto);

        $mensaje = ($save_contacto)? "Contacto guardado correctamente":$error;
        $this->json(
            array(
                "Resultado" => $save_contacto,
                "Mensaje" => $mensaje
            )
        );
    }

    public function delete(){

        parse_str(file_get_contents('php://input'), $_DELETE);
        $this->json(array(
                "Metodo" => "delete",
                "Request" => $_DELETE,
                "Segmentos" => Router::getInstance()->getSegmentos()
            )
        );
    }

}