<?php

class inicio extends Controlador
{

    public function index(){
        $parametros_vista["nombre"] = "Super Simple MVC";
        $parametros_vista["plantillas"] = $this->vista("inicio/plantillas",array(),true);

        $modeloEstados = $this->modelo("estadosModelo");

        $parametros_vista["estados"] = $modeloEstados->getAll();

        $parametros["vista"] = $this->vista("inicio/index/vista",$parametros_vista,true);

        $this->vista("html",$parametros);
    }

    function getMunicipios(){
        $segmentos = Router::getInstance()->getSegmentos();
        $id_estado = $segmentos[1];

        $modeloInicio = $this->modelo("inicioModelo");
        $municipios = $modeloInicio->getMunicipios($id_estado);
        $this->json(
            array(
                "Resultado" => $municipios
            )
        );
    }

    function adios(){
        echo "adios";
    }
}