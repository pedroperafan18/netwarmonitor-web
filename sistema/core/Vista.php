<?php

class Vista
{
    public static function cargarCss(){
        $temaObject = Config::getPropiedad("tema");
        $nombreTemaString = $temaObject->getNombreTema();
        $csstema = $temaObject->getCss();
        $htmlCss = "";
        foreach ($csstema as $css){
            if(file_exists("assets/".$nombreTemaString."/css/".$css.".css")){
                if(file_exists("assets/".$nombreTemaString."/css/".$css.".min.css")){
                    $cssFile = "assets/".$nombreTemaString."/css/".$css.".min.css";
                }else{
                    $cssFile = "assets/".$nombreTemaString."/css/".$css.".css";
                }
            }else{
                if(file_exists("assets/".$nombreTemaString."/css/".$css.".min.css")){
                    $cssFile = "assets/".$nombreTemaString."/css/".$css.".min.css";
                }
            }
            $htmlCss.='<link rel="stylesheet" href="'.Router::getUrl().$cssFile.'">'."\n";

        }
        return $htmlCss;
    }

    public static function cargarJs(){
        $temaObject = Config::getPropiedad("tema");
        $nombreTemaString = $temaObject->getNombreTema();
        $jstema = $temaObject->getJs();
        $htmlJs = "";
        foreach ($jstema as $js){
            if(file_exists("assets/".$nombreTemaString."/js/".$js.".js")){
                if(file_exists("assets/".$nombreTemaString."/js/".$js.".min.js")){
                    $jsFile = "assets/".$nombreTemaString."/js/".$js.".min.js";
                }else{
                    $jsFile = "assets/".$nombreTemaString."/js/".$js.".js";
                }
            }else{
                if(file_exists("assets/".$nombreTemaString."/js/".$js.".min.js")){
                    $jsFile = "assets/".$nombreTemaString."/js/".$js.".min.js";
                }
            }
            $htmlJs.='<script src="'.Router::getUrl().$jsFile.'"></script>'."\n";
        }
        return $htmlJs;
    }


}