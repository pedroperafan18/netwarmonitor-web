
$(document).ready(function(){

    $("#divRegresar").click(function(){
        $("#divOpciones").removeClass("hidden");
        $("#divReporte").addClass("hidden");
        $("#divCrearCita").addClass("hidden");
        $("#divCrearContacto").addClass("hidden");
        $("#divListaContactos").addClass("hidden");
        $(this).addClass("hidden");
    });

    $("#reporteCitas").click(function(){
        $("#divReporte").removeClass("hidden");
        $("#divRegresar").removeClass("hidden");
        $("#divOpciones").addClass("hidden");
    });

    $("#agregarCita").click(function(){
        $("#divCrearCita").removeClass("hidden");
        $("#divRegresar").removeClass("hidden");
        $("#divOpciones").addClass("hidden");

        $("#editarContacto").addClass("hidden");
        $("#guardarContacto").removeClass("hidden");

        $.ajax({
            url: "contacto",
            method: "GET"
        }).done(function(data){
            var html = "";
            html+="<option value='0'></option>";
            $.each(data.Resultado,function(index,item){
                var id = item.ID;
                html+="<option value='"+id+"'>"+item.Nombre+"</option>";
            });
            $("#Contacto").html(html);
        });
    });

    $("#generarReporte").click(function(e){
        e.preventDefault();

        var fechainicial = $("#FechaInicial").val();
        var fechafinal =$("#FechaFinal").val();
        $.ajax({
            url: "cita/reporte",
            method: "POST",
            data: {
                FechaInicial: fechainicial,
                FechaFinal: fechafinal
            }
        }).done(function(data){
            if(data.Resultado){
                $("#FechaInicial").val("");
                $("#FechaFinal").val("");

                var citas = data.Citas;
                var html = "";
                $.each(citas,function(index,item){
                    var plantilla = $("#templateCita").html();
                    plantilla = plantilla.replace(new RegExp("{{Contacto}}", 'g'),item.Nombre||"");
                    plantilla = plantilla.replace(new RegExp("{{Fecha}}", 'g'),item.Fecha||"");
                    plantilla = plantilla.replace(new RegExp("{{Hora}}", 'g'),item.Hora||"");
                    plantilla = plantilla.replace(new RegExp("{{Duracion}}", 'g'),item.Duracion||"");
                    plantilla = plantilla.replace(new RegExp("<(\/|)tbody>", 'g'),"");
                    html+=plantilla;
                });
                $("#resultadosReporte").html(html);
                $("#tablaResultadosReporte").removeClass("hidden");
            }else{
                alert(data.Mensaje);
            }
        });

    });

    $("#guardarCita").click(function(e){
        e.preventDefault();
       var duracion = $("#Duracion").val();
       var hora = $("#Hora").val();
       var fecha = $("#Fecha").val();
       var contacto = $("#Contacto").val();

        $.ajax({
            url: "cita",
            method: "POST",
            data: {
                Duracion: duracion,
                Hora: hora,
                Fecha:fecha,
                Contacto: contacto
            }
        }).done(function(data){
            console.log(data);
            if(data.Resultado){
                $("#Duracion").val("0");
                $("#Hora").val("");
                $("#Fecha").val("");
                $("#Contacto").val("0");
            }
            alert(data.Mensaje);
        });
    });

    $("#agregarContacto").click(function(){
        localStorage.clear();
        $("#divCrearContacto").removeClass("hidden");
        $("#divRegresar").removeClass("hidden");
        $("#divOpciones").addClass("hidden");

        $("#guardarContacto").removeClass("hidden");
        $("#editarContacto").addClass("hidden");

        $("#Nombre").val("");
        $("#Telefono").val("");
        $("#TipoTelefono").val("1");
        $("#Correo").val("");
        $("#Estado").val("");
        $("#Municipio").val("");
    });

    $("#editarContacto").click(function(e) {
        e.preventDefault();
        var id = localStorage.getItem("ID");
        agregar_editar("contacto/"+id,"PUT");
        //regresar
    });


    $("#guardarContacto").click(function(e){
        e.preventDefault();
        agregar_editar("contacto","POST");
    });

    function agregar_editar(url,metodo){
        var nombre = $("#Nombre").val();
        var telefono = $("#Telefono").val();
        var tipo_telefono = $("#TipoTelefono").val();
        var correo = $("#Correo").val();
        var estado = $("#Estado").val();
        var municipio = $("#Municipio").val();
        $.ajax({
            url: url,
            method: metodo,
            data: {
                Nombre: nombre,
                Telefono: telefono,
                TipoTelefono:tipo_telefono,
                Correo: correo,
                Estado: estado,
                Municipio: municipio
            }
        }).done(function(data){
            console.log(data);
            if(data.Resultado){
                $("#Nombre").val("");
                $("#Telefono").val("");
                $("#TipoTelefono").val("1");
                $("#Correo").val("");
                $("#Estado").val("");
                $("#Municipio").val("");
            }
            alert(data.Mensaje);
        });
    }

    $("#Estado").change(function(){
        var valor = $(this).val();
        getMunicipios(valor,null);
    });

    function getMunicipios(valor,select){
        $.ajax({
            url: "estado/"+valor,
            method: "GET"
        }).done(function(data){
            console.log(data);
            var html = "";
            html+="<option value='0'></option>";
            $.each(data.Resultado,function(index,item) {
                var id = item.id;
                var municipio = item.municipio;
                html+="<option value='"+id+"'>"+municipio+"</option>";
            });
            $("#Municipio").html(html);
            $("#Municipio").val(select);
        });
    }

    $("#aVerContactos").click(function(){

        $("#divListaContactos").removeClass("hidden");
        $("#divRegresar").removeClass("hidden");
        $("#divOpciones").addClass("hidden");
        $.ajax({
            url: "contacto",
            method: "GET"
        }).done(function(data){
            console.log(data);
            var html = "";
            $.each(data.Resultado,function(index,item){
                var plantilla = $("#templateContacto").html();
                plantilla = plantilla.replace(new RegExp("{{ID}}", 'g'),item.ID||"");
                plantilla = plantilla.replace(new RegExp("{{Nombre}}", 'g'),item.Nombre||"");
                plantilla = plantilla.replace(new RegExp("{{Correo}}", 'g'),item.Correo||"");
                plantilla = plantilla.replace(new RegExp("{{Telefono}}", 'g'),item.Telefono||"");
                plantilla = plantilla.replace(new RegExp("{{Estado}}", 'g'),item.Estado||"");
                plantilla = plantilla.replace(new RegExp("{{Municipio}}", 'g'),item.Municipio||"");
                plantilla = plantilla.replace(new RegExp("<(\/|)tbody>", 'g'),"");
                html+=plantilla;
            });
            $("#listaContactos").html(html);
        });
    });

    $("#tablaContactos input").keyup(function(){
        var index = $(this).parent().index();
        var nth = "#tablaContactos td:nth-child("+(index+1).toString()+")";
        var valor = $(this).val().toUpperCase();
        $("#tablaContactos tbody tr").show();
        $(nth).each(function(){
            if($(this).text().toUpperCase().indexOf(valor) < 0){
                $(this).parent().hide();
            }
        });
    });

    $("#tablaContactos input").blur(function(){
        $(this).val("");
    });

    $(document).on("click",".editar",function(){
        console.log($(this).data("id"));

        $("#divListaContactos").addClass("hidden");
        $("#divCrearContacto").removeClass("hidden");

        $("#editarContacto").removeClass("hidden");
        $("#guardarContacto").addClass("hidden");

        var id = $(this).data("id");
        $.ajax({
            url: "contacto/"+id,
            method: "GET"
        }).done(function(data){
            var contacto = data.Resultado;
            localStorage.setItem("ID",contacto.ID);
            $("#Nombre").val(contacto.Nombre);
            $("#Telefono").val(contacto.Telefono);
            $("#TipoTelefono").val(contacto.TipoTelefono);
            $("#Correo").val(contacto.Correo);
            $("#Estado").val(contacto.Estado);
            getMunicipios(contacto.Estado,contacto.Municipio);
        });
    });
});