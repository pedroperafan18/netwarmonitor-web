<?php

Router::setControladorDefault("inicio");

Router::addRuta('inicio',"inicio/index");

Router::addRuta('contacto',"contacto/create/","POST");
Router::addRuta('contacto',"contacto/getAll","GET");
Router::addRuta('contacto/[[:digit:]]',"contacto/getOne","GET");
Router::addRuta('contacto/[[:digit:]]',"contacto/update","PUT");
Router::addRuta('contacto/[[:digit:]]',"contacto/delete","DELETE");


Router::addRuta('estado/[[:digit:]]',"inicio/getMunicipios","GET");

Router::addRuta("cita","cita/create","POST");